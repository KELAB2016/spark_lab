# 2016년 지식공학연구실 Spark Seminar 자료 (참관자용) #


##** 발표 슬라이드 자료** ##

- [link](https://bitbucket.org/KELAB2016/spark_lab/src/54993be61fa48ed3bd3f0ac106fb7562e0b6e6de/slides/?at=master)
- 링크 클릭 -> 항목 클릭 -> raw 클릭으로 다운로드 

##** 설치 가이드 ** ##

- [link](https://bitbucket.org/KELAB2016/spark_lab/raw/4b20f6e04ea22378e85244082b0bf6d255834841/slides/Spark%20Seminar-%20Week%20one%20(%EC%8B%A4%EC%8A%B5%ED%99%98%EA%B2%BD%EA%B5%AC%EC%B6%95).pptx)

##** 무설치 실습환경(고려대학교 교내에서만 실행 가능) ** ##

- [link](http://163.152.111.34:8001)
- 실행만 해주세요 ^^ 소스코드 수정 시 충돌이 일어날 수 있습니다.
- 실습 완료후에는 세션을 shutdown해주세요 ^^
##** Source Code ** ##

- [link](https://bitbucket.org/KELAB2016/spark_lab/src/c89ede5d96664d43f8c5423f464b64ad1120d2ce/source/?at=master)